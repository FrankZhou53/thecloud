Shader "Unlit/BlendShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _SecondTex("Second Texture", 2D) = "white" {}
        _MaskTex("Mask Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _SecondTex;
            sampler2D _MaskTex;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col1 = tex2D(_MainTex, i.uv);
                fixed4 col2 = tex2D(_SecondTex, i.uv);

#if UNITY_UV_STARTS_AT_TOP
                float grabSign = -_ProjectionParams.x;
#else
                float grabSign = _ProjectionParams.x;
#endif

                half2 uv = float2(1, grabSign) * (i.uv - half2(0.5, 0.5)) + half2(0.5, 0.5);

                fixed4 mask = tex2D(_MaskTex, uv);
                return lerp(col1, col2, mask.a);
            }
            ENDCG
        }
    }
}