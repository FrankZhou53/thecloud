Shader "Custom/SpriteColorBlender" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _MainColor ("MainColor", Color) = (0.5,1,1,1)
        _Alphe ("Alphe", Range(0, 1)) = 1
        _SecondColor ("SecondColor", Color) = (1,0.5,0.5,1)
        _BlendValue ("BlendValue", Range(0, 1)) = 1
        _Color_Sat ("Color_Sat", Range(0, 3)) = 1.600605
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector]_Stencil ("Stencil ID", Float) = 0
        [HideInInspector]_StencilReadMask ("Stencil Read Mask", Float) = 255
        [HideInInspector]_StencilWriteMask ("Stencil Write Mask", Float) = 255
        [HideInInspector]_StencilComp ("Stencil Comparison", Float) = 8
        [HideInInspector]_StencilOp ("Stencil Operation", Float) = 0
        [HideInInspector]_StencilOpFail ("Stencil Fail Operation", Float) = 0
        [HideInInspector]_StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _MainColor;
            uniform float _Alphe;
            uniform float _BlendValue;
            uniform float4 _SecondColor;
            uniform float _Color_Sat;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float a = (_MainTex_var.a*_Alphe*i.vertexColor.a); // A
                float3 emissive = ((_MainTex_var.rgb*pow(((((i.uv0.g*_BlendValue)*-1.0+1.0)*_MainColor.rgb)+(((i.uv0.g*_BlendValue)*1.0+0.0)*_SecondColor.rgb)),_Color_Sat)*i.vertexColor.rgb)*a);
                float3 finalColor = emissive;
                return fixed4(finalColor,a);
            }
            ENDCG
        }
    }
    FallBack "Sprites/Default"
}
