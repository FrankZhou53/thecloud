﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UVMove" {
	Properties
	{ 		//[PerRendererData]
		_MainTex("Texture", 2D) = "white"{} //纹理 
		_ScrollXSpeed("MoveX",float) = 0.2
		_ScrollYSpeed("MoveY",float) = 0.2	
        
	} 		
		SubShader {
        Tags 
        { 
            "Queue"="Transparent"
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
        }
        
		Lighting Off

        LOD 200
        
        CGPROGRAM
        #pragma surface surf myLightModel alpha

        sampler2D _MainTex;
        fixed _ScrollXSpeed;
        fixed _ScrollYSpeed;
        
        struct Input {
            float2 uv_MainTex;
            fixed4 color : COLOR;
        };

        float4 LightingmyLightModel(SurfaceOutput s, float3 lightDir,half3 viewDir, half atten)
        {
            float4 c ; 
  		 	c.rgb =  s.Albedo;
  			c.a = s.Alpha; 
    		return c;
        }
 
        void surf (Input IN, inout SurfaceOutput o) {
            fixed2 scrolledUV = IN.uv_MainTex;
             
            fixed xScrollValue = _ScrollXSpeed * _Time.y;
            fixed yScrollValue = _ScrollYSpeed * _Time.y;
             
            scrolledUV += fixed2(xScrollValue, yScrollValue);
            
            IN.color=fixed4(1,1,1,1);
            half4 c = tex2D (_MainTex, scrolledUV);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG		
					
		}		
	FallBack "Diffuse"

}
