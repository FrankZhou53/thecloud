﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class GamingPanel : AbstractPanel
    {
        [SerializeField] private Button m_BtnRain;
        [SerializeField] private Button m_BtnThunder;
        [SerializeField] private Button m_BtnSnow;
        [SerializeField] private GameObject M_ObjTouchTips;
        private List<Transform> m_LstTrsBtn = new List<Transform>();
        private int timer = -1;
        private int index = 0;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnRain.onClick.AddListener(OnClickRain);
            m_BtnThunder.onClick.AddListener(OnClickThunder);
            m_BtnSnow.onClick.AddListener(OnClickSnow);
            HideAllBtn();
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            for (var i = 0; i < args.Length; i++)
            {
                ShowBtn((Define.ActionType)args[i]);
            }
            StartAnimBtn();
            EventSystem.S.Register(EventID.OnStartGame, OnStartGame);
        }

        protected override void OnClose()
        {
            base.OnClose();
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
            EventSystem.S.UnRegister(EventID.OnStartGame, OnStartGame);
        }
        void ShowBtn(Define.ActionType _type)
        {
            switch (_type)
            {
                case Define.ActionType.Rain:
                    m_BtnRain.gameObject.SetActive(true);
                    m_BtnRain.transform.Find("Tips").gameObject.SetActive(false);
                    m_LstTrsBtn.Add(m_BtnRain.transform);
                    break;
                case Define.ActionType.Thunder:
                    m_BtnThunder.gameObject.SetActive(true);
                    m_BtnThunder.transform.Find("Tips").gameObject.SetActive(false);
                    m_LstTrsBtn.Add(m_BtnThunder.transform);
                    break;
                case Define.ActionType.Snow:
                    m_BtnSnow.gameObject.SetActive(true);
                    m_BtnSnow.transform.Find("Tips").gameObject.SetActive(false);
                    m_LstTrsBtn.Add(m_BtnSnow.transform);
                    break;
                default:
                    break;
            }
        }
        void StartAnimBtn()
        {
            m_BtnRain.interactable = true;
            m_BtnThunder.interactable = true;
            m_BtnSnow.interactable = true;
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
            timer = Timer.S.Post2Really((int time) =>
            {
                for (var i = 0; i < m_LstTrsBtn.Count; i++)
                {
                    m_LstTrsBtn[i].Find("Tips").gameObject.SetActive(i == (index % m_LstTrsBtn.Count));
                }
                index++;
            }, 0.66f, -1);
        }
        void HideAllBtn()
        {
            m_LstTrsBtn.Clear();
            m_BtnRain.gameObject.SetActive(false);
            m_BtnThunder.gameObject.SetActive(false);
            m_BtnSnow.gameObject.SetActive(false);
        }
        void HideAnimBtn(Define.ActionType _type)
        {
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
            m_BtnRain.interactable = false;
            m_BtnThunder.interactable = false;
            m_BtnSnow.interactable = false;
            m_BtnRain.transform.Find("Tips").gameObject.SetActive(false);
            m_BtnThunder.transform.Find("Tips").gameObject.SetActive(false);
            m_BtnSnow.transform.Find("Tips").gameObject.SetActive(false);
            GameObject showTip = null;
            switch (_type)
            {
                case Define.ActionType.Rain:
                    showTip = m_BtnRain.transform.Find("Tips").gameObject;
                    break;
                case Define.ActionType.Thunder:
                    showTip = m_BtnThunder.transform.Find("Tips").gameObject;
                    break;
                case Define.ActionType.Snow:
                    showTip = m_BtnSnow.transform.Find("Tips").gameObject;
                    break;
                default:
                    break;
            }
            if (showTip != null)
            {
                Sequence quence = DOTween.Sequence();
                for (var i = 0; i < 3; i++)
                {
                    quence.AppendCallback(() =>
                    {
                        showTip.SetActive(true);
                    });
                    quence.AppendInterval(0.15f);
                    quence.AppendCallback(() =>
                    {
                        showTip.SetActive(false);
                    });
                    quence.AppendInterval(0.15f);
                }
                quence.AppendCallback(() =>
                {
                    HideAllBtn();
                    switch (_type)
                    {
                        case Define.ActionType.Rain:
                            EventSystem.S.Send(EventID.OnChooseRain);
                            break;
                        case Define.ActionType.Thunder:
                            EventSystem.S.Send(EventID.OnChooseThunder);
                            break;
                        case Define.ActionType.Snow:
                            EventSystem.S.Send(EventID.OnChooseSnow);
                            break;
                        default:
                            break;
                    }
                });
            }
            else
            {
                HideAllBtn();
            }
        }
        void OnClickRain()
        {
            HideAnimBtn(Define.ActionType.Rain);
        }
        void OnClickThunder()
        {
            HideAnimBtn(Define.ActionType.Thunder);
        }
        void OnClickSnow()
        {
            HideAnimBtn(Define.ActionType.Snow);
        }
        void OnStartGame(int Key, params object[] args)
        {
            M_ObjTouchTips.SetActive(false);
        }
    }
}