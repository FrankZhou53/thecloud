﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class GameWinPanel : AbstractPanel
    {
        [SerializeField] private Button m_BtnNext;
        [SerializeField] private Button m_BtnRetry;

        private int timer = -1;
        private int index = 0;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnNext.onClick.AddListener(OnClickNext);
            m_BtnRetry.onClick.AddListener(OnClickRetry);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);

        }

        protected override void OnClose()
        {
            base.OnClose();
        }
        void OnClickNext()
        {
            if ((int)PlayerInfoMgr.data.currentLevel == (int)SceneEnum.Level1)
            {
                SceneLogicMgr.S.ChangeScene(SceneEnum.Level0, false);
            }
            else
            {
                SceneLogicMgr.S.ChangeScene(SceneEnum.Level1, false);
            }
            CloseSelfPanel();
        }
        void OnClickRetry()
        {
            SceneLogicMgr.S.ChangeScene((SceneEnum)(int)PlayerInfoMgr.data.currentLevel, false);
            CloseSelfPanel();
        }

    }
}