﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class SettingPanel : AbstractAnimPanel
    {
        [SerializeField] private Text m_TxtVer;
        [SerializeField] private Toggle m_TogBtnSound;
        [SerializeField] private Button m_BtnWarn;
        [SerializeField] private Button m_BtnPrivacy;
        [SerializeField] private Button m_BtnClose;


        protected override void OnUIInit()
        {
            base.OnUIInit();

            m_TogBtnSound.isOn = AudioMgr.S.isSoundEnable;
            m_TogBtnSound.onValueChanged.AddListener(OnSoundStateChanged);
            m_BtnWarn.onClick.AddListener(OnClickWarn);
            m_BtnPrivacy.onClick.AddListener(OnClickPrivacy);

            m_BtnClose.onClick.AddListener(OnClickClose);

            m_BtnWarn.gameObject.SetActive(false);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            OpenDependPanel(EngineUI.MaskPanel, -1, null);
            m_TxtVer.text = string.Format("v{0}", Application.version);
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

        void OnClickWarn()
        {

        }

        void OnClickPrivacy()
        {
            Application.OpenURL("https://www.ptowin.com/en/privacy_policy.htm");
        }

        void OnSoundStateChanged(bool isOn)
        {
            AudioMgr.S.isMusicEnable = isOn;
            AudioMgr.S.isSoundEnable = isOn;
        }

        void OnClickClose()
        {
            HideSelfWithAnim();
        }
    }
}

