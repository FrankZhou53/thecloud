﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class RulePanel : AbstractAnimPanel
    {
        [SerializeField] private Text m_TxtRule;
        [SerializeField] private Button m_BtnClose;


        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnClose.onClick.AddListener(OnClickClose);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            OpenDependPanel(EngineUI.MaskPanel, -1, null);
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

        void OnClickClose()
        {
            HideSelfWithAnim();
        }
    }
}

