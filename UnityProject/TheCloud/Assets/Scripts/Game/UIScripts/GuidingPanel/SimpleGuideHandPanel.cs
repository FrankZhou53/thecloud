﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class SimpleGuideHandPanel : AbstractPanel
    {
        [SerializeField]
        private Transform m_TrsGuide;

        private Transform m_TrsFollow;
        private Vector3 m_WorldOffset;

        protected override void OnUIInit()
        {
            base.OnUIInit();
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            if (args != null && args.Length > 2)
            {
                if ((bool)args[0])
                {
                    CustomExtensions.ScenePosition2UIPosition(Camera.main, UIMgr.S.uiRoot.uiCamera, (Vector3)args[1], m_TrsGuide);
                }
                else
                {
                    m_TrsGuide.position = (Vector3)args[1];
                }
                m_TrsGuide.localPosition += (Vector3)args[2];


                if (args.Length > 3)
                {
                    m_TrsFollow = (Transform)args[3];
                }

                if (args.Length > 4)
                {
                    m_WorldOffset = (Vector3)args[4];
                }
            }
        }

        protected override void OnClose()
        {
            base.OnClose();
            m_TrsFollow = null;
            m_WorldOffset = Vector3.zero;
        }

        void LateUpdate()
        {
            if (m_TrsFollow != null)
                m_TrsGuide.position = m_TrsFollow.position + m_WorldOffset;
        }
    }
}
