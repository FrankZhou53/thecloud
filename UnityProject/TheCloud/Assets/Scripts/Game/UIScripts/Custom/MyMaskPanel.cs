﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using DG.Tweening;

namespace Qarth
{
    public class MyMaskPanel : AbstractPanel
    {
        [SerializeField]
        private RawImage m_BgImage;
        protected override void OnOpen()
        {
            m_BgImage.color = new Color(m_BgImage.color.r, m_BgImage.color.g, m_BgImage.color.b, 0);
            m_BgImage.DOFade(0.8f, 0.4f);
        }

        protected override void OnClose()
        {
            m_BgImage.DOFade(0, 0.4f);
        }
    }
}
