﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class UIContentScaler : MonoBehaviour
    {
        private float m_DefaultRatio;
        private int m_LastHeight;

        private Vector2 m_OriginSize;

        public bool scaleSubRoot = true;
        public bool lockX = false;

        void Awake()
        {
            m_DefaultRatio = 9 / 16f;
            m_OriginSize = transform.rectTransform().sizeDelta;
        }

        void Update()
        {
            if (m_LastHeight != Screen.height)
            {
                m_LastHeight = Screen.height;
                ResizeUIContent();
            }
        }

        void ResizeUIContent()
        {
            var newRatio = Screen.width * 1.0f / Screen.height;
            if (lockX)
                transform.rectTransform().sizeDelta
                    = new Vector2(m_OriginSize.x, m_OriginSize.y * m_DefaultRatio / newRatio);
            else
                transform.rectTransform().sizeDelta = m_OriginSize * m_DefaultRatio / newRatio;

            if (scaleSubRoot && transform.childCount == 1)
                transform.GetChild(0).localScale = Vector3.one * m_DefaultRatio / newRatio;
        }
    }
}
