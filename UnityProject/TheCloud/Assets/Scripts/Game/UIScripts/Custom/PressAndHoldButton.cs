﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Qarth;


namespace GameWish.Game
{
    public class PressAndHoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        private bool m_IsPointDown;
        private float m_PointDownTimer;

        public UnityEvent_Object onHoldEnough;
        public UnityEvent_Object onClick;
        public UnityEvent onPointerUp;

        public bool interactable = true;

        [SerializeField]
        public float m_RequireHoldTime = 0.4f;

        [SerializeField]
        public Image m_ImgTarget;
        [SerializeField]
        private Sprite m_SprDisable;

        [SerializeField]
        private Image m_ImgFill;

        [SerializeField]
        private float m_PressScale = 0.95f;

        private Vector3 m_ScaleOrigin;
        private Sprite m_SprOrigin;

        private bool m_LastInterState;



        void Awake()
        {
            m_ScaleOrigin = transform.localScale;
            m_LastInterState = interactable;
            if (m_ImgTarget == null)
                m_ImgTarget = GetComponent<Image>();

            m_SprOrigin = m_ImgTarget.sprite;
        }

        void Update()
        {
            if (m_IsPointDown)
            {
                m_PointDownTimer += Time.deltaTime;
                if (m_PointDownTimer > m_RequireHoldTime)
                {
                    if (onHoldEnough != null)
                    {
                        onHoldEnough.Invoke(gameObject);
                    }
                    Reset();
                }
                if (m_ImgFill != null)
                    m_ImgFill.fillAmount = m_PointDownTimer / m_RequireHoldTime;
            }

            if (m_LastInterState != interactable)
            {
                if (m_ImgTarget != null && m_SprDisable != null)
                {
                    m_LastInterState = interactable;
                    m_ImgTarget.sprite = interactable ? m_SprOrigin : m_SprDisable;
                }
            }
        }

        void Reset()
        {
            m_IsPointDown = false;
            m_PointDownTimer = 0;
            if (m_ImgFill != null)
                m_ImgFill.fillAmount = 0;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!interactable)
                return;

            m_IsPointDown = true;
            m_PointDownTimer = 0;

            transform.localScale = m_ScaleOrigin * m_PressScale;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!interactable)
                return;

            Reset();
            transform.localScale = m_ScaleOrigin;

            if (onPointerUp != null)
                onPointerUp.Invoke();
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            if (!interactable)
                return;

            if (onClick != null)
            {
                onClick.Invoke(gameObject);
            }
        }
    }
}