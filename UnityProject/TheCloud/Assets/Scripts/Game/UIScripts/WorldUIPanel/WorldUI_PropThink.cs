using DG.Tweening;
using Qarth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameWish.Game;

public class WorldUI_PropThink : WorldUIBindTransform
{
    [SerializeField] private Transform m_TrsThing;
    void Awake()
    {
        transform.localScale = Vector3.zero;
    }

    public void SetValue()
    {
        transform.DOKill();
        transform.localScale = Vector3.zero;
        transform.DOScale(1, 0.1f);
        Sequence seq = DOTween.Sequence();
        seq.Append(m_TrsThing.DOScale(1, 0.35f));
        seq.Append(m_TrsThing.DOScale(0.9f, 0.35f));
        seq.SetLoops(-1);
    }

    public void Recycle()
    {
        GameObjectPoolMgr.S.Recycle(gameObject);
    }
}
