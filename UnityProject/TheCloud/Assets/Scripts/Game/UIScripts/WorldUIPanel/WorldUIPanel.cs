﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{

    public class WorldUIPanel : AbstractPanel
    {
        public static WorldUIPanel S;
        [SerializeField] private WorldUI_PropAmaze m_PropAmaze;
        [SerializeField] private WorldUI_PropDanger m_PropDanger;
        [SerializeField] private WorldUI_PropThink m_PropThink;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            S = this;

            GameObjectPoolMgr.S.AddPool("WorldUI_PropAmaze", m_PropAmaze.gameObject, -1, 3);
            GameObjectPoolMgr.S.AddPool("WorldUI_PropDanger", m_PropDanger.gameObject, -1, 3);
            GameObjectPoolMgr.S.AddPool("WorldUI_PropThink", m_PropThink.gameObject, -1, 3);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        public WorldUI_PropAmaze AllocatePropAmazeUI(Transform position)
        {
            var go = GameObjectPoolMgr.S.Allocate("WorldUI_PropAmaze").GetComponent<WorldUI_PropAmaze>();

            if (go != null)
            {
                go.followTransform = position;

                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.SetValue();
            }
            return go;
        }
        public WorldUI_PropDanger AllocatePropDangerUI(Transform position)
        {
            var go = GameObjectPoolMgr.S.Allocate("WorldUI_PropDanger").GetComponent<WorldUI_PropDanger>();

            if (go != null)
            {
                go.followTransform = position;

                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.SetValue();
            }
            return go;
        }
        public WorldUI_PropThink AllocatePropThinkUI(Transform position)
        {
            var go = GameObjectPoolMgr.S.Allocate("WorldUI_PropThink").GetComponent<WorldUI_PropThink>();

            if (go != null)
            {
                go.followTransform = position;

                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.SetValue();
            }
            return go;
        }
    }
}
