using DG.Tweening;
using Qarth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameWish.Game;

public class WorldUI_PropAmaze : WorldUIBindTransform
{
    [SerializeField] private List<Transform> m_LstAmaze = new List<Transform>();
    void Awake()
    {
        transform.localScale = Vector3.zero;
        for (var i = 0; i < m_LstAmaze.Count; i++)
        {
            m_LstAmaze[i].localScale = Vector3.zero;
        }
    }

    public void SetValue()
    {
        transform.DOKill();
        transform.localScale = Vector3.zero;
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(1, 1f));
        for (var i = 0; i < m_LstAmaze.Count; i++)
        {
            m_LstAmaze[i].localScale = Vector3.zero;
            seq.Append(m_LstAmaze[i].DOScale(1, 0.2f));
        }
        seq.AppendInterval(1.5f);
        seq.AppendCallback(() =>
        {
            Recycle();
        });
    }

    public void Recycle()
    {
        GameObjectPoolMgr.S.Recycle(gameObject);
    }
}
