using DG.Tweening;
using Qarth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameWish.Game;

public class WorldUI_PropDanger : WorldUIBindTransform
{
    [SerializeField] Transform m_TrsTips;
    void Awake()
    {
        transform.localScale = Vector3.zero;
    }

    public void SetValue()
    {
        transform.DOKill();
        transform.localScale = Vector3.zero;

        transform.DOScale(1, 0.1f);
        Sequence seq = DOTween.Sequence();
        seq.Append(m_TrsTips.DOScale(1, 1.5f));
        seq.Append(m_TrsTips.DOScale(0.8f, 0.15f));
        seq.SetLoops(-1);
    }

    public void Recycle()
    {
        GameObjectPoolMgr.S.Recycle(gameObject);
    }
}
