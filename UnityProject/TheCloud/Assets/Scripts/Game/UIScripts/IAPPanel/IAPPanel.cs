﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class IAPPanel : AbstractAnimPanel
    {
        [Header("MyFields")]
        [SerializeField] private Button m_BtnCloseUI = null;
        [SerializeField] private Toggle[] m_Toggles;

        [SerializeField]
        private Text m_Title;
        [SerializeField]
        private Text m_GemTabText;
        [SerializeField]
        private Text m_MemberTabText;
        [SerializeField]
        private Text m_ProTabText;

        protected override void OnUIInit()
        {
            base.OnUIInit();
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);

            m_BtnCloseUI.onClick.AddListener(OnClickCloseBtn);
        }

        protected override void OnClose()
        {
            base.OnClose();
            m_BtnCloseUI.onClick.RemoveAllListeners();
        }


        private void OnClickCloseBtn()
        {
            CloseSelfPanel();
        }

        public void ChangeToggle(int index)
        {
            m_Toggles[index].isOn = true;
        }
    }

}