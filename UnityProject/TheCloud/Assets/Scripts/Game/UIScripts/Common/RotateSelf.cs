﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateSelf : MonoBehaviour
{
    private Tweener m_Anim = null;

    public void StartRotate()
    {
        if (m_Anim == null)
            m_Anim = transform.DOLocalRotate(new Vector3(0, 0, 360), 0.5f, RotateMode.FastBeyond360).SetLoops(-1);
    }

    public void EndRotate()
    {
        if (m_Anim != null)
        {
            transform.DOKill();
            m_Anim = null;
        }
    }


}
