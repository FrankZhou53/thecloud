﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class Anim
    {
        public static Sequence PlayBreathEffect(Transform transform, float duration, float delay, int loop = -1)
        {
            Vector3 scale = transform.localScale;
            Vector3 beginScale = scale * 0.95f;
            transform.localScale = beginScale;
            return DOTween.Sequence()
                .Append(transform.DOScale(scale * 1.1f, duration).SetEase(Ease.InOutSine))
                //.AppendInterval(duration * 0.2f)
                .Append(transform.DOScale(beginScale, duration).SetEase(Ease.InOutSine))
                .SetLoops(loop)
                .SetEase(Ease.Linear)
                .SetDelay(delay);
        }

        public static Sequence PlayJellyEffect(Transform transform, float s, float dur, int loop = -1)
        {
            Vector3 scale = transform.localScale;
            return DOTween.Sequence()
            .Append(transform.DOScale(scale * s, 0.1f))
            .Append(transform.DOScale(scale, 0.1f))
            .SetEase(Ease.InOutBounce)
            .SetLoops(loop);
        }

        public static Sequence PlayShakeEffect(Transform transform, float dur)
        {
            Vector3 scale = Vector3.one;
            return DOTween.Sequence()
           .Append(transform.DOScale(scale * 1.1f, dur))
           .Append(transform.DOScale(scale, dur))
           .SetEase(Ease.OutBack)
           .SetLoops(1);
        }

        public static Sequence PlayRotateEffect(Transform transform, float degree, float duration, int loop = -1)
        {
            Quaternion qua = transform.localRotation;
            return DOTween.Sequence()
            .Append(transform.DOLocalRotate(new Vector3(0, 0, degree), duration, RotateMode.LocalAxisAdd))
            .Append(transform.DOLocalRotate(qua.eulerAngles, duration))
            .Append(transform.DOLocalRotate(new Vector3(0, 0, -degree), duration, RotateMode.LocalAxisAdd))
            .Append(transform.DOLocalRotate(qua.eulerAngles, duration))
            .SetLoops(loop);
        }

        public static Sequence PlayLargenEffect(Transform transform, float duration)
        {
            transform.localScale = Vector3.zero;
            Image image = transform.GetComponent<Image>();
            image.color = new Color(1, 1, 1, 0);
            image.DOFade(0.3f, duration);

            return DOTween.Sequence()
            .Append(transform.DOScale(Vector3.one * 1.3f, duration))
            .Append(image.DOFade(0, duration))
            .SetEase(Ease.Linear)
            .SetLoops(1);
        }

        public static Sequence PlayMoveX(Transform transform, float endPos, float duration)
        {
            //Vector3 startPos = transform.localPosition;
            return DOTween.Sequence()
            .Append(transform.DOLocalMoveX(endPos, duration))
            .SetEase(Ease.Linear)
            .SetLoops(-1);
        }

        public static Sequence PlayMoveY(Transform transform, float endPos, float duration)
        {
            transform.DOKill();
            Vector3 startPos = transform.localPosition;
            return DOTween.Sequence()
            .Append(transform.DOLocalMoveY(endPos, duration))
            .SetEase(Ease.Linear)
            .SetLoops(1);
        }

        public static Sequence PlayMoveUpDown(Transform transform, float range, float duration)
        {
            Vector3 startPos = transform.localPosition;
            return DOTween.Sequence()
            .Append(transform.DOLocalMoveY(startPos.y + range, duration))
            .Append(transform.DOLocalMoveY(startPos.y, duration))
            .Append(transform.DOLocalMoveY(startPos.y - range, duration))
            .Append(transform.DOLocalMoveY(startPos.y, duration))
            .SetEase(Ease.Linear)
            .SetLoops(-1);
        }

        public static Sequence PlayPingPongMoveX(Transform transform, float range, float duration)
        {

            Vector3 startPos = transform.localPosition;
            return DOTween.Sequence()
            .Append(transform.DOLocalMoveX(startPos.x + range, duration))
            .Append(transform.DOLocalMoveX(startPos.x, duration))
            .Append(transform.DOLocalMoveX(startPos.x - range, duration))
            .Append(transform.DOLocalMoveX(startPos.x, duration))
            .SetEase(Ease.Linear)
            .SetLoops(-1);
            //.SetDelay(0.3f);
        }

        public static Sequence PlayPingPongMoveY(Transform transform, float range, float duration)
        {

            Vector3 startPos = transform.localPosition;
            return DOTween.Sequence()
            .Append(transform.DOLocalMoveY(startPos.y + range, duration))
            .Append(transform.DOLocalMoveY(startPos.y, duration))
            .Append(transform.DOLocalMoveY(startPos.y - range, duration))
            .Append(transform.DOLocalMoveY(startPos.y, duration))
            .SetEase(Ease.Linear)
            .SetLoops(-1)
            /*.SetDelay(0.3f)*/;
        }

        public static Sequence PlayShine(Image image, float dur, float delay, float scale)
        {
            image.gameObject.transform.localScale = Vector3.zero;

            image.color = new Color(1, 1, 1, 0);
            image.DOFade(1, dur);

            return DOTween.Sequence()
            .Append(image.gameObject.transform.DOScale(Vector3.one, dur))
            .Append(image.DOFade(0, dur))
            .SetEase(Ease.InOutQuad)
            .SetLoops(-1)
            .SetDelay(delay);
        }
    }
}
