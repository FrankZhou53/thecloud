﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using EZCameraShake;
using DG.Tweening;

namespace GameWish.Game
{
    public class RenderCamMgr : TMonoSingleton<RenderCamMgr>
    {
        public Camera renderCam;
        public Transform transFreaksRoot;
        private ResLoader m_ResLoader;

        public Sprite GetRenderSprite()
        {
            if (renderCam == null)
                return null;
            var ptr = renderCam.targetTexture.GetNativeTexturePtr();
            var tex2D = Texture2D.CreateExternalTexture(
                        renderCam.targetTexture.width, renderCam.targetTexture.height,
                        TextureFormat.ARGB32, false, false, ptr);
            tex2D.UpdateExternalTexture(ptr);
            // rtCam.Render();
            // var tex2D = rtCam.targetTexture.toTexture2D();
            return Sprite.Create(tex2D,
                    new Rect(0, 0, renderCam.targetTexture.width, renderCam.targetTexture.height),
                    new Vector2(0.5f, 0.5f));
        }

        public Texture GetTexture()
        {
            if (renderCam == null)
                return null;

            return renderCam.targetTexture;
        }

        public GameObject LoadFreak(string _Name)
        {
            return LoadRes(_Name, transFreaksRoot);
        }


        public void ReleaseRes(string _Name)
        {
            m_ResLoader.ReleaseRes(_Name);
        }
        private GameObject LoadRes(string _Name, Transform _Parent)
        {
            if (m_ResLoader == null)
            {
                m_ResLoader = ResLoader.Allocate(this.GetType().Name);
            }
            var prefab = m_ResLoader.LoadSync(_Name);
            var go = Instantiate(prefab, _Parent) as GameObject;
            go.name = _Name;
            return go;
        }
    }
}
