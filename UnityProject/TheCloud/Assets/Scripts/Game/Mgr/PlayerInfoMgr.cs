﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class PlayerInfoMgr : DataClassHandler<GameData>
    {
        public static EventSystem itemEventSystem = new EventSystem();
        public static DataDirtyRecorder dataDirtyRecorder = new DataDirtyRecorder();

        public PlayerInfoMgr()
        {
            Load();
            EnableAutoSave();
        }
    }
}
