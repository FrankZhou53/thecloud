﻿using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class MovieMgr : MonoBehaviour//TMonoSingleton<MovieMgr>
    {
        [SerializeField] private Transform m_TrsCamera;
        [SerializeField] private Transform m_TrsTarget;
        public float moveSpeed = 7;
        public float boundary = 14;
        public Vector3 offset;
        private Vector3 start_pos;
        private bool isAnim = false;
        private bool isGameFinishStart = false;

        // Start is called before the first frame update
        void Start()
        {
            isAnim = false;
            isGameFinishStart = false;
            //offset = m_TrsCamera.position - m_TrsTarget.position;
            m_TrsCamera.SetZ(m_TrsTarget.position.z + offset.z);
        }

        // Update is called once per frame
        void Update()
        {
            if (!isAnim)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    start_pos = new Vector3(Camera.main.pixelRect.width / 2, Camera.main.pixelRect.height / 2, 0);//Input.mousePosition;
                    //Log.e(Camera.main.pixelRect);
                    EventSystem.S.Send(EventID.OnStartGame);
                }
                else if (Input.GetMouseButton(0))
                {
                    Vector3 forward = (Input.mousePosition - start_pos).normalized;
                    forward = new Vector3(forward.x, 0, forward.y);
                    m_TrsTarget.Translate(forward * moveSpeed * Time.deltaTime);
                    if (m_TrsTarget.position.x > boundary)
                        m_TrsTarget.SetX(boundary);
                    if (m_TrsTarget.position.x < -boundary)
                        m_TrsTarget.SetX(-boundary);
                    if (m_TrsTarget.position.z > boundary)
                        m_TrsTarget.SetZ(boundary);
                    if (m_TrsTarget.position.z < -boundary)
                        m_TrsTarget.SetZ(-boundary);
                }
                else if (Input.GetMouseButtonUp(0))
                {

                }
            }
            if (!isGameFinishStart)
            {
                m_TrsCamera.SetZ(m_TrsTarget.position.z + offset.z);
                m_TrsCamera.LookAt(m_TrsTarget);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawRay(m_TrsCamera.position, (m_TrsTarget.position - m_TrsCamera.position));
        }
        public void SetTarget(Transform _target)
        {
            m_TrsTarget = _target;
            isGameFinishStart = true;
            isAnim = true;
            Sequence quence = DOTween.Sequence();
            quence.Append(m_TrsCamera.DOMoveZ(m_TrsTarget.position.z + offset.z, 1));
            quence.Append(m_TrsCamera.DOLookAt(m_TrsTarget.position, 1));
            quence.AppendCallback(() =>
            {
                isGameFinishStart = false;
            });
            quence.Append(m_TrsCamera.DOMoveY(20, 1f));
        }
        public void CameraDown()
        {
            isAnim = true;
        }
    }
}
