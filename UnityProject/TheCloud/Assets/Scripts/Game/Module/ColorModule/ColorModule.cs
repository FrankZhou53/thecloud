﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Qarth;

namespace GameWish.Game
{
    public class ColorModule : TSingleton<ColorModule>
    {
        private ColorConfig m_Config;
        private CustomColorConfig m_CustomConfig;

        public void Init()
        {
            Log.i("Init ColorModule.");
        }

        public ColorConfig colorConfig
        {
            get { return m_Config; }
        }

        public override void OnSingletonInit()
        {
            m_Config = LoadConfig();
            m_CustomConfig = LoadCustomConfig();
        }

        public Color GetColor(float precent)
        {
            return ColorGradientHelper.CalcualteColor(m_Config.colors, precent);
        }

        private ColorConfig LoadConfig()
        {
            ResLoader loader = ResLoader.Allocate("ColorModule", null);

            UnityEngine.Object obj = loader.LoadSync("ColorConfig");
            if (obj == null)
            {
                Log.e("Not Find Color Config.");
                loader.Recycle2Cache();
                return null;
            }

            //Log.i("Success Load SDK Config.");
            ColorConfig prefab = obj as ColorConfig;

            ColorConfig newAB = GameObject.Instantiate(prefab);

            loader.Recycle2Cache();

            return newAB;
        }

        private CustomColorConfig LoadCustomConfig()
        {
            ResLoader loader = ResLoader.Allocate("ColorModule", null);

            UnityEngine.Object obj = loader.LoadSync("CustomColorConfig");
            if (obj == null)
            {
                Log.e("Not Find Color Config.");
                loader.Recycle2Cache();
                return null;
            }

            //Log.i("Success Load SDK Config.");
            CustomColorConfig prefab = obj as CustomColorConfig;

            CustomColorConfig newAB = GameObject.Instantiate(prefab);

            loader.Recycle2Cache();

            return newAB;
        }

        public Color GetStarColor(int index)
        {
            return m_CustomConfig.StarColors[index];
        }

        public Color GetBtnColor(int index)
        {
            return m_CustomConfig.BtnColors[index];
        }

        public Color GetEnergyColor(int index)
        {
            return m_CustomConfig.EnergyColor[index];
        }

        public Color GetSignPrefabColor(int index)
        {
            return m_CustomConfig.SignPrefabColor[index];
        }

        public Color GetGradeColor(int index)
        {
            return m_CustomConfig.GradeColor[index];
        }
        public Color GetTrashColor(int index)
        {
            return m_CustomConfig.TrashColor[index];
        }
        public Color GetBtnBuyColor(int index)
        {
            return m_CustomConfig.BtnColor[index];
        }
    }
}
