using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Qarth;


namespace GameWish.Game
{
    public class TaskStartTrigger : ITrigger
    {
        private int m_NeedTaskId;
        protected Action<bool, ITrigger> m_Listener;

        public bool isReady
        {
            get
            {
                return false;//PlayerInfoMgr.data.taskData.CheckOwnTaskId(m_NeedTaskId);
            }
        }
        public void SetParam(object[] param)
        {
            m_NeedTaskId = int.Parse(param[0].ToString());
        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_Listener = l;
            EventSystem.S.Register(EventID.OnTaskAdd, OnEventListerner);
        }

        public void Stop()
        {
            m_Listener = null;
            EventSystem.S.UnRegister(EventID.OnTaskAdd, OnEventListerner);
        }

        private void OnEventListerner(int key, params object[] args)
        {
            if (m_Listener == null)
            {
                return;
            }

            if (isReady)
            {
                m_Listener(true, this);
                m_Listener = null;
            }
            else
            {
                m_Listener(false, this);
            }


        }
    }
}
