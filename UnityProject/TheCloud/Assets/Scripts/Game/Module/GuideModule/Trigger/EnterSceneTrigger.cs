using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class EnterSceneTrigger : ITrigger
    {
        protected Action<bool, ITrigger> m_Listener;

        private SceneEnum m_CurScene;
        private SceneEnum m_TarScene;

        public bool isReady
        {
            get
            {
                return m_CurScene == m_TarScene;
            }
        }
        public void SetParam(object[] param)
        {
            if (param != null && param.Length > 0)
                m_TarScene = CustomExtensions.GetStringEnum<SceneEnum>(param[0].ToString());
        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_Listener = l;
            EventSystem.S.Register(EventID.OnSceneLoaded, OnEventListerner);
            EventSystem.S.Register(EngineEventID.OnPanelUpdate, OnEventListerner);

        }

        public void Stop()
        {
            m_Listener = null;
            EventSystem.S.UnRegister(EventID.OnSceneLoaded, OnEventListerner);
            EventSystem.S.UnRegister(EngineEventID.OnPanelUpdate, OnEventListerner);
        }

        private void OnEventListerner(int key, params object[] args)
        {
            if (m_Listener == null)
            {
                return;
            }
            m_CurScene = SceneLogicMgr.S.CurSceneEnum;
            m_Listener(true, this);
        }
    }
}

