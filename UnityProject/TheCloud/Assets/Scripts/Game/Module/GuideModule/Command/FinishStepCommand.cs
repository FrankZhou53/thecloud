using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class FinishStepCommand : AbstractGuideCommand
    {
        private IUINodeFinder m_Finder;
        private Transform m_TargetNode;
        private bool m_Finish = false;

        public override void SetParam(object[] pv)
        {
            if (pv != null && pv.Length > 0)
            {
                m_Finder = pv[0] as IUINodeFinder;
            }
        }

        protected override void OnStart()
        {
            m_TargetNode = m_Finder.FindNode(false);
            if (m_TargetNode == null)
                m_Finish = true;
            else
                AppLoopMgr.S.onUpdate += Update;
        }

        protected override void OnFinish(bool forceClean)
        {
            AppLoopMgr.S.onUpdate -= Update;
        }

        private void Update()
        {
            if (!m_Finish)
            {
                if (m_TargetNode.gameObject.activeSelf)
                {
                    m_Finish = true;
                    FinishStep();
                }
            }
        }
    }
}
