﻿using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
	public class WaitCommand : AbstractGuideCommand
	{
	    private float m_WaitTime;
        public override void SetParam(object[] pv)
        {
            if (pv==null || pv.Length == 0)
            {
                Log.w("GuideHandCommand Init With Invalid Param.");
                return;
            }

            m_WaitTime = float.Parse(pv[0].ToString());
        }
        protected override void OnStart()
        {
            Timer.S.Post2Really((count) => { FinishStep(); }, m_WaitTime);
        }

        protected override void OnFinish(bool forceClean)
        {
        }
    }
}

