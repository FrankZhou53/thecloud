﻿using System.Collections;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class UIDataModule : AbstractModule
    {
        public static void RegisterStaticPanel()
        {
            InitUIPath();
            UIDataTable.SetABMode(false);
            UIDataTable.AddPanelData(UIID.LogoPanel, null, "LogoPanel/LogoPanel");
            UIDataTable.AddPanelData(UIID.SplashPanel, null, "LogoPanel/SplashPanel");
        }

        protected override void OnComAwake()
        {
            InitUIPath();
            RegisterAllPanel();
        }

        private static void InitUIPath()
        {
            PanelData.PREFIX_PATH = "Resources/UI/Panels/{0}";
            PageData.PREFIX_PATH = "Resources/UI/Panels/{0}";
        }

        private void RegisterAllPanel()
        {
            UIDataTable.SetABMode(true);

            UIDataTable.AddPanelData(EngineUI.FloatMessagePanel, null, "Common/FloatMessagePanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.MsgBoxPanel, null, "Common/MsgBoxPanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.HighlightMaskPanel, null, "Guide/HighlightMaskPanel", true, 0);
            UIDataTable.AddPanelData(EngineUI.GuideHandPanel, null, "Guide/GuideHandPanel", true, 0);
            UIDataTable.AddPanelData(EngineUI.MaskPanel, null, "Common/MaskPanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.ColorFadeTransition, null, "Common/ColorFadeTransition", true, 1);
            UIDataTable.AddPanelData(SDKUI.AdDisplayer, null, "Common/AdDisplayer", false, 1);
            UIDataTable.AddPanelData(SDKUI.OfficialVersionAdPanel, null, "OfficialVersionAdPanel");

            UIDataTable.AddPanelData(UIID.GuideWordsPanel, null, "GamePanel/GuidePanel/GuideWordsPanel");
            UIDataTable.AddPanelData(UIID.WorldUIPanel, null, "GamePanel/WorldUIPanel/WorldUIPanel", true);
            UIDataTable.AddPanelData(EngineUI.RatePanel, null, "GamePanel/RatePanel", true);
            UIDataTable.AddPanelData(UIID.LoadingPanel, null, "GamePanel/LoadingPanel/LoadingPanel", true, 1);

            UIDataTable.AddPanelData(UIID.GamingPanel, null, "GamePanel/MainPanel/GamingPanel", true);
            UIDataTable.AddPanelData(UIID.GameWinPanel, null, "GamePanel/GamingPanel/GameWinPanel", true);
            UIDataTable.AddPanelData(UIID.GameLosePanel, null, "GamePanel/GamingPanel/GameLosePanel", true);
            // UIDataTable.AddPanelData(UIID.BalloonPanel, null, "GamePanel/BalloonPanel/BalloonPanel", true);
            // UIDataTable.AddPanelData(UIID.SlotPanel, null, "GamePanel/SlotPanel/SlotPanel", true);
            // UIDataTable.AddPanelData(UIID.SlotAgainPanel, null, "GamePanel/SlotAgainPanel/SlotAgainPanel", true);
            UIDataTable.AddPanelData(UIID.MainPanel, null, "GamePanel/MainPanel/MainPanel", true);
            // UIDataTable.AddPanelData(UIID.RulePanel, null, "GamePanel/RulePanel/RulePanel", true);
            // UIDataTable.AddPanelData(UIID.OfflinePanel, null, "GamePanel/OfflinePanel/OfflinePanel", true);
            // UIDataTable.AddPanelData(UIID.SettingPanel, null, "GamePanel/SettingPanel/SettingPanel", true);

            // UIDataTable.AddPanelData(UIID.LevelFailedPanel, null, "GamePanel/LevelFailedPanel/LevelFailedPanel", true);
            // UIDataTable.AddPanelData(UIID.LevelPassPanel, null, "GamePanel/LevelPassPanel/LevelPassPanel", true);

            // UIDataTable.AddPanelData(UIID.RedpackGetPanel, null, "GamePanel/RedpackPanel/RedpackGetPanel", true);

            // UIDataTable.AddPanelData(UIID.TopBarPanel, null, "GamePanel/TopBarPanel/TopBarPanel", true);
            // UIDataTable.AddPanelData(UIID.PlayerInfoPanel, null, "GamePanel/PlayerInfoPanel/PlayerInfoPanel", true);
            // UIDataTable.AddPanelData(UIID.TaskPanel, null, "GamePanel/TaskPanel/TaskPanel", true);

            // UIDataTable.AddPanelData(UIID.SignInPanel, null, "GamePanel/SignInPanel/SignInPanel", true);

            // UIDataTable.AddPanelData(UIID.GetRewardPanel, null, "GamePanel/GetRewardPanel/GetRewardPanel", true);
            // UIDataTable.AddPanelData(UIID.GuideButtonClickPanel, null, "Panels/Guide/GuideButtonClickPanel", true);
            // UIDataTable.AddPanelData(UIID.MyGuideWordsPanel, null, "Panels/Guide/MyGuideWordsPanel", true);


            // UIDataTable.AddPanelData(UIID.SimpleGuideHandPanel, null, "Panels/Guide/SimpleGuideHandPanel", true);

            // UIDataTable.AddPanelData(UIID.ToolsPanel, null, "GamePanel/ToolsPanel/ToolsPanel", true);

            // UIDataTable.AddPanelData(UIID.RemoveBlockPanel, null, "GamePanel/BonusPanel/RemoveBlockPanel", true);
            // UIDataTable.AddPanelData(UIID.SupplyBallPanel, null, "GamePanel/BonusPanel/SupplyBallPanel", true);
            // UIDataTable.AddPanelData(UIID.DirectHitPanel, null, "GamePanel/BonusPanel/DirectHitPanel", true);

            // UIDataTable.AddPanelData(UIID.RedeemWithdrawPanel, null, "GamePanel/RedeemWithdrawPanel/RedeemWithdrawPanel", true);
            // UIDataTable.AddPanelData(UIID.RedeemPanel, null, "GamePanel/RedeemPanel/RedeemPanel", true);
            // UIDataTable.AddPanelData(UIID.RedeemSorryPanel, null, "GamePanel/RedeemSorryPanel/RedeemSorryPanel", true);
            // UIDataTable.AddPanelData(UIID.RedeemRecordPanel, null, "GamePanel/RedeemRecordPanel/RedeemRecordPanel", true);
            // UIDataTable.AddPanelData(UIID.RedeemTipsPanel, null, "GamePanel/RedeemTipsPanel/RedeemTipsPanel", true);

            // UIDataTable.AddPanelData(UIID.CollectionPanel, null, "GamePanel/CollectionPanel/CollectionPanel", true);
        }

    }
}