﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum UIID
    {
        LogoPanel = 0,
        //测试工具界面
        ToolsPanel,
        SplashPanel,
        GuideWordsPanel,
        LoadingPanel,
        WorldUIPanel,
        MainPanel,
        //引导点击界面
        GuideButtonClickPanel,
        //引导提示字界面
        MyGuideWordsPanel,

        GamingPanel,
        GameWinPanel,
        GameLosePanel,
    }
}
