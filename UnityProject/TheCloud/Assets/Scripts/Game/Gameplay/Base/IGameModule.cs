using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
namespace GameWish.Game
{
    public interface IGameModule
    {
        void Init(ISceneCtrller ctrller);
        void Tick(float deltaTime);
        void Clean();

        void RegisterEvts();
        void UnRegisterEvts();
    }
}