﻿using System.Security.AccessControl;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PropDriftwood : PropBase
    {
        [SerializeField] private GameObject m_ObjFire;
        [SerializeField] private Animator m_AnimRole;
        [SerializeField] private GameObject m_ObjEfft1;
        [SerializeField] private GameObject m_ObjEfft2;
        public bool alive = true;
        private int timer = -1;
        protected override void Init()
        {
            m_ObjFire.SetActive(false);
            m_AnimRole.SetBool("isTexting", true);
        }
        public override void ActionRain()
        {
            if (alive)
            {

            }
        }
        public override void ActionThunder()
        {
            if (alive)
            {
                alive = false;
                m_ObjFire.SetActive(true);
                m_AnimRole.SetBool("isTexting", false);
                m_AnimRole.SetBool("isDead", true);
                EventSystem.S.Send(EventID.OnPropActionDriftwoodDestory);
            }
        }
        public override void ActionSnow()
        {
            if (alive)
            {

            }
        }
        private void OnDestroy()
        {
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
        }
        public void WinAnim()
        {
            m_AnimRole.SetBool("isTexting", false);
            m_AnimRole.SetBool("isWaving", true);
            m_ObjEfft1.SetActive(true);
        }
        public void ShowCoin()
        {
            m_ObjEfft2.SetActive(true);
        }
    }
}
