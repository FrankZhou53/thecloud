﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PropBase : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Init();
        }

        // Update is called once per frame
        void Update()
        {

        }
        protected virtual void Init()
        {

        }
        public virtual void ActionRain()
        {

        }
        public virtual void ActionThunder()
        {

        }
        public virtual void ActionSnow()
        {

        }
    }
}
