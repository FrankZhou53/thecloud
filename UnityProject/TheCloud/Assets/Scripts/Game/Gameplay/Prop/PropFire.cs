﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PropFire : PropBase
    {
        [SerializeField] private GameObject m_ObjFire;
        [SerializeField] private GameObject m_ObjNoFire;
        public override void ActionRain()
        {
            Extinguish();
        }
        private void Extinguish()
        {
            m_ObjFire.SetActive(false);
            m_ObjNoFire.SetActive(true);
            //然后冒个烟
        }
    }
}
