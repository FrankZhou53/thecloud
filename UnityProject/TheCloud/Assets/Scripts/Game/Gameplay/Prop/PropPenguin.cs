﻿using System.Security.AccessControl;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PropPenguin : PropBase
    {
        [SerializeField] private Animation m_Anim;
        private GameObject m_ObjUI;
        public bool alive = true;
        protected override void Init()
        {
            Idle();
            Timer.S.CallWithDelay(() =>
            {
                m_ObjUI = WorldUIPanel.S.AllocatePropThinkUI(transform).gameObject;
            }, 0.2f);
        }
        public override void ActionRain()
        {

        }
        public override void ActionThunder()
        {

        }
        public override void ActionSnow()
        {

        }
        public void Idle()
        {
            m_Anim.PlayAnim("penguin_idle");
        }
        public void Struggle()
        {
            m_Anim.PlayAnim("penguin_struggle");
            if (m_ObjUI != null && m_ObjUI.GetComponent<WorldUI_PropThink>() != null)
            {
                m_ObjUI.GetComponent<WorldUI_PropThink>().Recycle();
            }
            m_ObjUI = WorldUIPanel.S.AllocatePropDangerUI(transform).gameObject;
        }
        public void Move()
        {
            m_Anim.PlayAnim("penguin_move");
        }
        public void Win()
        {
            m_Anim.PlayAnim("penguin_win");
        }
        public void HideTips()
        {
            if (m_ObjUI != null && m_ObjUI.GetComponent<WorldUI_PropDanger>() != null)
            {
                m_ObjUI.GetComponent<WorldUI_PropDanger>().Recycle();
            }
            if (m_ObjUI != null && m_ObjUI.GetComponent<WorldUI_PropThink>() != null)
            {
                m_ObjUI.GetComponent<WorldUI_PropThink>().Recycle();
            }
        }
        private void OnDestroy()
        {
            HideTips();
        }
    }
}
