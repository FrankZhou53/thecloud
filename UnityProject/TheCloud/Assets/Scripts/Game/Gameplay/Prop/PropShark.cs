﻿using System.Security.AccessControl;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PropShark : PropBase
    {
        [SerializeField] private Animation m_Anim;
        [SerializeField] private GameObject m_ObjBlood;
        [SerializeField] private Transform m_TrsGetPos;
        public bool alive = true;
        private int timer = -1;
        protected override void Init()
        {
            m_ObjBlood.SetActive(false);
            Idle();
        }
        public override void ActionRain()
        {
            if (alive)
            {

            }
        }
        public override void ActionThunder()
        {
            if (alive)
            {
                m_Anim.PlayAnim("shark_hurt");
                alive = false;
            }
        }
        public override void ActionSnow()
        {
            if (alive)
            {

            }
        }
        public void Attack(GameObject _target)
        {
            m_Anim.PlayAnim("shark_attack");
            Timer.S.CallWithDelay(() =>
            {
                timer = Timer.S.Post2Really((int time) =>
                {
                    m_ObjBlood.SetActive(false);
                    m_ObjBlood.SetActive(true);
                }, 0.3f, -1);
            }, 0.07f);
            Timer.S.CallWithDelay(() =>
            {
                _target.transform.parent = m_TrsGetPos;
                _target.transform.localPosition = Vector3.zero;
                _target.transform.localEulerAngles = Vector3.zero;
                //_target.SetActive(false);
            }, 0.15f);
            Timer.S.CallWithDelay(() =>
            {
                Idle();
            }, 0.17f);
        }
        public void Idle()
        {
            m_Anim.PlayAnim("shark_idle");
        }
        private void OnDestroy()
        {
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
        }
    }
}
