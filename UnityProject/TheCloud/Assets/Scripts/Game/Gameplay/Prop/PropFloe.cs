﻿using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PropFloe : PropBase
    {
        [SerializeField] private Transform m_TrsMovePos;
        [SerializeField] private GameObject m_ObjFloe;
        [SerializeField] private GameObject m_ObjFloeBreak;
        public bool isHide = false;
        protected override void Init()
        {
            if (isHide)
            {
                this.transform.Find("floe").localScale = Vector3.zero;
            }
            m_ObjFloe.SetActive(true);
            m_ObjFloeBreak.SetActive(false);
        }
        public override void ActionRain()
        {

        }
        public override void ActionThunder()
        {
            //碎裂
            if (!isHide)
            {
                isHide = true;
                //this.transform.DOScale(Vector3.zero, 0.2f);
                m_ObjFloe.SetActive(false);
                m_ObjFloeBreak.SetActive(true);
                EventSystem.S.Send(EventID.OnPropActionFloeDestory);
            }
        }
        public override void ActionSnow()
        {
            if (isHide)
            {
                this.transform.Find("floe").DOScale(1.5f, 1.0f);
                isHide = false;
                EventSystem.S.Send(EventID.OnPropActionFloeCreate);
            }
        }
    }
}
