﻿using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PropRiver : PropBase
    {
        public float max_y = 1;
        private float tile = 0;
        private bool success = false;
        protected override void Init()
        {
            success = false;
        }
        public override void ActionRain()
        {
            tile += Time.deltaTime / 2;
            if (tile <= max_y)
            {
                this.transform.SetY(tile);
            }
            else
            {
                if (!success)
                {
                    Log.e("max");
                    success = true;
                    EventSystem.S.Send(EventID.OnPropActionRiverMax);
                }
            }
        }
        public override void ActionThunder()
        {

        }
        public override void ActionSnow()
        {

        }
    }
}
