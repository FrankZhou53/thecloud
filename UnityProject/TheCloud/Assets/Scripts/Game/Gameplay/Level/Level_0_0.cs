﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class Level_0_0 : MonoBehaviour
    {
        [SerializeField] private MovieMgr m_MovieMgr;
        [SerializeField] private List<PropFloe> m_LstPropFloe = new List<PropFloe>();
        [SerializeField] private PropFloe m_StartPropFloe;
        [SerializeField] private PropPenguin m_TrsPenguin;
        [SerializeField] private PropShark m_TrsShark;
        [SerializeField] private List<Transform> m_LstTrsMovePos = new List<Transform>();
        [SerializeField] private Transform m_TrsSharkPos;
        private int targetCount = 0;
        void Start()
        {
            PlayerInfoMgr.data.SetLvl((int)SceneEnum.Level0);
            targetCount = m_LstPropFloe.Count;
            EventSystem.S.Register(EventID.OnPropActionFloeCreate, OnPropActionFloeCreate);
            EventSystem.S.Register(EventID.OnPropActionFloeDestory, OnPropActionFloeDestory);

            UIMgr.S.OpenPanel(UIID.GamingPanel, Define.ActionType.Thunder, Define.ActionType.Snow);
        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnDestroy()
        {
            EventSystem.S.UnRegister(EventID.OnPropActionFloeCreate, OnPropActionFloeCreate);
            EventSystem.S.UnRegister(EventID.OnPropActionFloeDestory, OnPropActionFloeDestory);
        }
        void OnPropActionFloeCreate(int key, params object[] args)
        {
            targetCount -= 1;
            if (targetCount == 0)
            {
                DataAnalysisMgr.S.CustomEvent(string.Format(Define.DataAnalysisEvent.EVT_LEVEL_COMPLETE, this.name));

                m_MovieMgr.CameraDown();
                //通关开始动画
                Sequence quence = DOTween.Sequence();
                quence.AppendInterval(1);
                quence.AppendCallback(() =>
                {
                    EventSystem.S.Send(EventID.OnFinishGame);
                    m_MovieMgr.SetTarget(m_TrsPenguin.transform);
                });
                quence.AppendInterval(2);
                quence.Append(m_TrsPenguin.transform.DOLookAt(m_LstTrsMovePos[0].position, 0.2f));
                quence.AppendCallback(() =>
                {
                    m_TrsPenguin.Move();
                    m_TrsPenguin.HideTips();
                });
                for (var i = 0; i < m_LstTrsMovePos.Count; i++)
                {
                    var target = m_LstTrsMovePos[i];
                    quence.AppendCallback(() =>
                    {
                        m_TrsPenguin.transform.LookAt(target);
                    });
                    quence.Append(m_TrsPenguin.transform.DOMove(m_LstTrsMovePos[i].position, 1.5f).SetEase(Ease.Linear));
                }
                quence.Append(m_TrsPenguin.transform.DOLookAt(m_TrsSharkPos.position, 0.3f));
                quence.AppendCallback(() =>
                {
                    m_TrsPenguin.Win();
                });
                quence.Append(m_TrsShark.transform.DOLookAt(m_TrsSharkPos.position, 0.2f));
                quence.Append(m_TrsShark.transform.DOMove(m_TrsSharkPos.position, 1f));
                quence.AppendCallback(() =>
                {
                    //成功
                    UIMgr.S.OpenTopPanel(UIID.GameWinPanel, null);
                });

            }
        }
        void OnPropActionFloeDestory(int key, params object[] args)
        {
            DataAnalysisMgr.S.CustomEvent(string.Format(Define.DataAnalysisEvent.EVT_LEVEL_FAIL, this.name));
            m_MovieMgr.CameraDown();
            //失败动画
            m_TrsPenguin.Struggle();
            Sequence quence = DOTween.Sequence();
            quence.Append(m_TrsPenguin.transform.DOMove(m_TrsPenguin.transform.position - new Vector3(0, 0.6f, 0), 0.4f));
            quence.AppendCallback(() =>
            {
                EventSystem.S.Send(EventID.OnFinishGame);
            });
            if (m_TrsShark.alive)
            {
                quence.AppendCallback(() =>
                {
                    //视角转到鲨鱼
                    m_MovieMgr.SetTarget(m_TrsShark.transform);
                    WorldUIPanel.S.AllocatePropAmazeUI(m_TrsShark.transform);
                });
                quence.AppendInterval(2);
                quence.Append(m_TrsShark.transform.DOMove(m_TrsPenguin.transform.position - new Vector3(3.5f, 0, 0), 2f));
                quence.AppendInterval(0.5f);
                quence.AppendCallback(() =>
                {
                    m_TrsShark.Attack(m_TrsPenguin.gameObject);
                    m_TrsPenguin.HideTips();
                    //m_TrsPenguin.gameObject.SetActive(false);
                });
                quence.AppendInterval(2);
            }
            else
            {
                quence.AppendInterval(2);
            }
            quence.AppendCallback(() =>
            {
                //结算界面
                UIMgr.S.OpenTopPanel(UIID.GameLosePanel, null);
            });
        }
    }
}
