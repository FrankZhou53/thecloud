﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class Level_0_1 : MonoBehaviour
    {
        [SerializeField] private MovieMgr m_MovieMgr;
        [SerializeField] private PropDriftwood m_DriftwoodMan;
        [SerializeField] private PropDriftwood m_DriftwoodWeman;
        [SerializeField] private Transform m_TrsPosMan;
        [SerializeField] private Transform m_TrsPosWeman;
        private int loseCount = 0;

        void Start()
        {
            PlayerInfoMgr.data.SetLvl((int)SceneEnum.Level1);
            loseCount = 0;

            EventSystem.S.Register(EventID.OnPropActionRiverMax, OnPropActionRiverMax);
            EventSystem.S.Register(EventID.OnPropActionDriftwoodDestory, OnPropActionDriftwoodDestory);

            UIMgr.S.OpenPanel(UIID.GamingPanel, Define.ActionType.Thunder, Define.ActionType.Rain);
        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnDestroy()
        {
            EventSystem.S.UnRegister(EventID.OnPropActionRiverMax, OnPropActionRiverMax);
            EventSystem.S.UnRegister(EventID.OnPropActionDriftwoodDestory, OnPropActionDriftwoodDestory);
        }
        void OnPropActionRiverMax(int key, params object[] args)
        {
            DataAnalysisMgr.S.CustomEvent(string.Format(Define.DataAnalysisEvent.EVT_LEVEL_COMPLETE, this.name));
            m_MovieMgr.CameraDown();
            //通关开始动画
            Sequence quence = DOTween.Sequence();
            quence.AppendInterval(1);
            quence.AppendCallback(() =>
            {
                EventSystem.S.Send(EventID.OnFinishGame);
                m_MovieMgr.SetTarget(m_TrsPosMan);
                Sequence quenceMan = DOTween.Sequence();
                quenceMan.Append(m_DriftwoodMan.transform.DOMove(m_TrsPosMan.position, 3f));
                m_DriftwoodMan.transform.DOLocalRotateQuaternion(Quaternion.Euler(180, 90, 90), 3f);

                Sequence quenceWeman = DOTween.Sequence();
                quenceWeman.Append(m_DriftwoodWeman.transform.DOMove(m_TrsPosWeman.position, 2f));
                //m_DriftwoodWeman.transform.DOLocalRotateQuaternion(Quaternion.Euler(0, 90, 90), 3f);
                //quenceWeman.Append(m_DriftwoodWeman.transform.DOLocalRotate(new Vector3(10, 90, 90), 0.2f));
                m_DriftwoodMan.WinAnim();
                m_DriftwoodWeman.WinAnim();
            });
            quence.AppendInterval(3);
            quence.AppendCallback(() =>
            {
                m_DriftwoodMan.ShowCoin();
                m_DriftwoodWeman.ShowCoin();
            });
            quence.AppendInterval(2);
            quence.AppendCallback(() =>
            {
                //成功
                UIMgr.S.OpenTopPanel(UIID.GameWinPanel, null);
            });
        }
        void OnPropActionDriftwoodDestory(int key, params object[] args)
        {
            loseCount += 1;
            //if (loseCount == 2)
            {
                DataAnalysisMgr.S.CustomEvent(string.Format(Define.DataAnalysisEvent.EVT_LEVEL_FAIL, this.name));
                m_MovieMgr.CameraDown();
                //失败动画
                Sequence quence = DOTween.Sequence();
                quence.AppendInterval(1);
                quence.AppendCallback(() =>
                {
                    EventSystem.S.Send(EventID.OnFinishGame);
                });
                quence.AppendInterval(2);
                quence.AppendCallback(() =>
                {
                    //结算界面
                    UIMgr.S.OpenTopPanel(UIID.GameLosePanel, null);
                });
            }
        }
    }
}
