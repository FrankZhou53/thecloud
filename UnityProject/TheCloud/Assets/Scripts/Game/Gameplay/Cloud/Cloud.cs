﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class Cloud : MonoBehaviour
    {
        [SerializeField] private GameObject m_ObjRain;
        [SerializeField] private GameObject m_ObjThunder;
        [SerializeField] private GameObject m_ObjSnow;
        public float radius = 0;
        public float height = 0;
        public bool action = false;
        // Start is called before the first frame update
        void Start()
        {
            OnFinishGame(0);
            EventSystem.S.Register(EventID.OnChooseRain, OnChooseRain);
            EventSystem.S.Register(EventID.OnChooseThunder, OnChooseThunder);
            EventSystem.S.Register(EventID.OnChooseSnow, OnChooseSnow);
            EventSystem.S.Register(EventID.OnFinishGame, OnFinishGame);
        }
        void OnDestroy()
        {
            EventSystem.S.UnRegister(EventID.OnChooseRain, OnChooseRain);
            EventSystem.S.UnRegister(EventID.OnChooseThunder, OnChooseThunder);
            EventSystem.S.UnRegister(EventID.OnChooseSnow, OnChooseSnow);
            EventSystem.S.UnRegister(EventID.OnFinishGame, OnFinishGame);
        }

        // Update is called once per frame
        void Update()
        {
            if (action)
            {
                Collider[] cols = Physics.OverlapCapsule(this.transform.position, this.transform.position - new Vector3(0, height, 0), radius);
                if (cols.Length > 0)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        var prop = cols[i].GetComponent<PropBase>();
                        if (prop != null)
                        {
                            if (m_ObjRain.activeSelf)
                            {
                                prop.ActionRain();
                            }
                            if (m_ObjThunder.activeSelf)
                            {
                                prop.ActionThunder();
                            }
                            if (m_ObjSnow.activeSelf)
                            {
                                prop.ActionSnow();
                            }
                        }
                    }
                }
            }
        }
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(this.transform.position, radius);
            Gizmos.DrawWireSphere(this.transform.position - new Vector3(0, height, 0), radius);
        }
        void OnChooseRain(int Key, params object[] args)
        {
            m_ObjRain.SetActive(true);
            Timer.S.CallWithDelay(() =>
            {
                action = true;
            }, 1);
        }
        void OnChooseThunder(int Key, params object[] args)
        {
            m_ObjThunder.SetActive(true);
            Timer.S.CallWithDelay(() =>
            {
                action = true;
            }, 0.5f);
        }
        void OnChooseSnow(int Key, params object[] args)
        {
            m_ObjSnow.SetActive(true);
            Timer.S.CallWithDelay(() =>
            {
                action = true;
            }, 1);
        }
        void OnFinishGame(int Key, params object[] args)
        {
            action = false;
            m_ObjRain.SetActive(false);
            m_ObjThunder.SetActive(false);
            m_ObjSnow.SetActive(false);
        }
    }
}
