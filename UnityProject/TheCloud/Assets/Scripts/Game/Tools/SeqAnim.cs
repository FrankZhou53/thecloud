﻿//=====================================================
// - FileName:      SeqAnim.cs
// - Author:       #AuthorName#
// - CreateTime:    #CreateTime#
// - Email:         #AuthorEmail#
// - Description:   
// -  (C) Copyright 2019, webeye,Inc.
// -  All Rights Reserved.
//======================================================
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Qarth;

namespace GameWish.Game
{
    public class SeqAnim : MonoBehaviour
    {
        Sequence m_Seq;
        public enum AnimType
        {
            Rotate,
            Scale,
        }
        public AnimType m_Type = AnimType.Rotate;
        private void OnEnable()
        {
            if (m_Seq != null)
            {
                m_Seq.Kill();
            }
            m_Seq = m_Type == AnimType.Rotate ? DOTween.Sequence().Append(this.transform.DOPunchRotation(new Vector3(0, 0, 15), 1f, 4)) : DOTween.Sequence().Append(this.transform.DOScale(1.05f, 0.5f)).Append(this.transform.DOScale(1, 0.5f));
            m_Seq.SetLoops(-1);
        }

        private void OnDisable()
        {
            m_Seq.Kill();
        }
    }
}

