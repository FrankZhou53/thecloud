﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Qarth;


namespace GameWish.Game
{

    public class CustomRandomHelper
    {
        public static int seed = 31;
        static CustomRandomHelper()
        {
            seed = (int)DateTime.Now.Ticks;
        }

        public static float Range(float min, float max, float step)
        {
            seed = 214013 * seed + 2531011;

            int maxLevel = (int)((max - min) / step);
            int level = RandomHelper.Range(0, maxLevel + 1);

            return min + step * level;
        }

        public static int Range(int min, int max, int step=1)
        {
            seed = 214013 * seed + 2531011;

            int maxLevel = (max - min) / step;
            int level = RandomHelper.Range(0, maxLevel + 1);

            return min + step * level;
        }
    }


}
