﻿#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;

public class iOSScreen : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_NeedModPos;

    private void Awake()
    {
        if (CalcNeedFix())
        {
            //m_NeedModPos.
            m_NeedModPos.anchoredPosition = new Vector3(0, -120, 0);
        }
        else
        {
            m_NeedModPos.anchoredPosition = Vector3.zero;
        }
    }

    public static bool CalcNeedFix()
    {
        bool needFix = false;
        switch (Device.generation)
        {
            case DeviceGeneration.iPhone7Plus:
            case DeviceGeneration.iPhone6Plus:
            case DeviceGeneration.iPhone6SPlus:
                {
                    needFix = false;
                }
                break;
            case DeviceGeneration.iPhoneX:
            case DeviceGeneration.iPhoneXS:
            case DeviceGeneration.iPhoneXSMax:
            case DeviceGeneration.iPhoneXR:
                {
                    needFix = true;
                }
                break;
            case DeviceGeneration.iPhoneUnknown:
            case DeviceGeneration.Unknown:
                {
                    if (Screen.height * 1.0f / Screen.width > 2)
                    {
                        needFix = true;
                    }
                }
                break;

        }

        return needFix;
    }
}
#endif