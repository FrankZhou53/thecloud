using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PosRendererSorter : MonoBehaviour
    {
        [SerializeField]
        private Renderer[] m_Renders;
        [SerializeField]
        private int m_SortOrderBase = -3000;
        [SerializeField]
        private float m_OffsetY;
        [SerializeField]
        private bool m_RunOnce;

        private float m_Timer;
        private float m_DeltaCheckTime = 0.1f;

        void Awake()
        {
            if (m_Renders == null || m_Renders.Length == 0)
                m_Renders = gameObject.GetComponentsInChildren<Renderer>();
        }

        void LateUpdate()
        {
            m_Timer -= Time.deltaTime;
            if (m_Timer < 0)
            {
                CheckSortingOrder();
            }

            if (m_RunOnce)
                Destroy(this);
        }

        public void Init()
        {
            if (m_Renders == null || m_Renders.Length == 0)
                m_Renders = gameObject.GetComponentsInChildren<Renderer>();
            CheckSortingOrder();
        }

        public void CheckSortingOrder()
        {
            if (m_Renders == null)
                return;
            for (int i = 0; i < m_Renders.Length; i++)
            {
                if (m_Renders[i] is ParticleSystemRenderer)
                    continue;
                m_Renders[i].sortingOrder = (int)(m_SortOrderBase - transform.position.y * 100 - m_OffsetY * 100);
            }
        }

        [ExecuteInEditMode]
        void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position + m_OffsetY * Vector3.up, .25f);
        }
    }
}