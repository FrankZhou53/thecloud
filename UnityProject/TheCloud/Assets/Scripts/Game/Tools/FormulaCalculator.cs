using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System.Text;

namespace GameWish.Game
{
    public class FormulaCaculator
    {
        public static BigInteger CalcFormulaVal(int type, int lvl, BigInteger[] args)
        {
            if (args == null)
                return 0;

            BigInteger val = new BigInteger("0");
            switch (type)
            {
                case 1:
                    {
                        if (args.Length < 2)
                            return val;
                        val = lvl * args[1] + args[0];
                    }
                    break;
                case 2://40 50 80 130
                    {
                        if (args.Length < 3)
                            return val;
                        for (int i = lvl - 1; i >= 0; i--)
                        {
                            val += i * args[2] + args[1];
                        }
                        val += args[0];
                    }
                    break;
                case 3:
                    {
                        if (args.Length < 3)
                            return val;
                        var tmpFloat = Mathf.Pow(lvl + 1, float.Parse(args[1].ToString()));
                        val = (new BigInteger(tmpFloat.ToString()) + args[2]) * args[0];
                    }
                    break;
                case 4:
                    {
                        if (args.Length < 2)
                            return val;
                        //菱形十二面数
                        for (int i = lvl; i >= 0; i--)
                        {
                            var tmpFloat = 4 * Mathf.Pow(i, 3) + 6 * Mathf.Pow(i, 2) + 4 * i + 1;
                            val += args[1] * new BigInteger(tmpFloat.ToString());
                        }
                        val += args[0];
                    }
                    break;

                // case 6:// 初始数值*增长数值^（等级-1）
                //     {
                //         if (args.Length < 2)
                //             return val;

                //         int varTemp = (int)Mathf.Pow(lvl - 1, int.Parse(args[1].ToString()));
                //         val = int.Parse(args[0].ToString()) * varTemp;
                //     }
                //     break;
                case 7:// 初始值+增长数值*（等级-1）+增量加成
                    {
                        if (args.Length < 3)
                            return val;

                        val = args[0] + args[1] * (lvl - 1) + args[2] * ((lvl - 1) * (lvl - 2)) / 2;

                    }
                    break;
            }
            return val;
        }
    }
}