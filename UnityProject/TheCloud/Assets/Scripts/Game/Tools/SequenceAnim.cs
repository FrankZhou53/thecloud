﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{

    public class SequenceAnim : MonoBehaviour
    {
        [SerializeField]
        private Transform m_TrsPlay;
        private float m_Duration = 0.3f;
        private float m_Angle = 0f;
        Sequence m_Sequene = null;
        Sequence m_Sequene2 = null;

        void OnEnable()
        {
            if (m_TrsPlay == null)
                m_TrsPlay = transform;
            Kill();
            m_Sequene = Anim.PlayPingPongMoveY(m_TrsPlay, 0.4f, m_Duration);
            m_Sequene2 = DOTween.Sequence()
                           .Append(m_TrsPlay.DOLocalRotate(new Vector3(0, 0, m_Angle), m_Duration, RotateMode.LocalAxisAdd))
                           .Append(m_TrsPlay.DOLocalRotate(Vector3.zero, m_Duration))
                           .Append(m_TrsPlay.DOLocalRotate(new Vector3(0, 0, -m_Angle), m_Duration, RotateMode.LocalAxisAdd))
                           .Append(m_TrsPlay.DOLocalRotate(Vector3.zero, m_Duration))
                           .SetLoops(-1);
        } 
        void Kill()
        {
            if (m_Sequene != null)
            {
                m_Sequene.Kill();
                m_Sequene = null;
            }
            if (m_Sequene2 != null)
            {
                m_Sequene2.Kill();
                m_Sequene2 = null;
            }
        }


    }

}