﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum EventID
    {
        //guide
        OnPanelClose,
        OnLanguageTableSwitchFinish,
        OnTaskAdd,
        ShowGuideLine,
        AddWord,
        UnLockPicBtn,
        OnGuidePropertyComplete,
        TaskStateChanged,

        //ui
        OnPropertyAdd,
        OnCardGet,
        OnCollectionGet,

        //scene
        OnSceneLoaded,

        OnStartGame,
        OnFinishGame,
        OnChooseRain,
        OnChooseThunder,
        OnChooseSnow,
        //交互道具事件定义
        OnPropActionFloeCreate,
        OnPropActionFloeDestory,
        OnPropActionDriftwoodDestory,
        OnPropActionRiverMax,
    }
}
