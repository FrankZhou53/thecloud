﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class Define
    {
        public const string FIRST_BONUS = "first_bonus";
        public const string SOUND_DEFAULT_SOUND = "Click";
        public const int LONG_SCREEN_OFFSET_TOP = 60;
        public const int LONG_SCREEN_OFFSET_BOTTOM = 15;

        #region GameEvent
        public const string START_GAME = "StartGaming";

        public const string BUZZ_STATE = "Buzz_State";
        public const string SHAKE_STATE = "Shake_State";
        #endregion

        //event
        public const string EVT_SHARE = "ShareGameLink";
        //SPRITE  NAME 
        public const string AD_PLACEMENT_REWARD = "MainReward";
        public const string AD_PLACEMENT_INTER = "MainInter";
        public const string AD_PLACEMENT_LEVEL_MIXVIEW = "LevelMixView";
        public const string AD_PLACEMENT_INFO_MIXVIEW = "InfoMixView";
        public const string AD_PLACEMENT_FAIL_MIXVIEW = "FailMixView";
        public const string AD_PLACEMENT_RED_MIXVIEW = "RedMixView";
        public const string AD_PLACEMENT_FAKE_RED_MIXVIEW = "FakeRedMixView";
        public const string AD_PLACEMENT_FAKE_LEVEL_MIXVIEW = "FakeLevelMixView";
        public const string AD_PLACEMENT_ACT_MIXVIEW = "ActMixView";

        public const string SIGN_DAY_KEY = "Gameplay_StartDate";



        public const float SAFETY_CASH_COUNT = 99.98f;
        public const int SAFETY_CARD_COUNT = 999;
        public const int SlOT_CARD_COUNT = 50;


        public const int BALLOON_STAGE_LVL = 5;

        public enum ActionType
        {
            None,
            Rain,
            Thunder,
            Snow,
        }
        public enum PropTipsType
        {
            None,
            Danger,
            Amaze,
        }
        public class DataAnalysisEvent
        {
            public const string EVT_LEVEL_COMPLETE = "evt_level_complete_{0}";
            public const string EVT_LEVEL_FAIL = "evt_level_fail_{0}";
        }
    }
}
