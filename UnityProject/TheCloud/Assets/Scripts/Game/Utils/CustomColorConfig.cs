﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Qarth
{
    [System.Serializable]
    public class CustomColorConfig : ScriptableObject
    {
        [SerializeField]
        public Color[] StarColors;
        [SerializeField]
        public Color[] BtnColors;
        [SerializeField]
        public Color[] EnergyColor;
        [SerializeField]
        public Color[] SignPrefabColor;
        [SerializeField]
        public Color[] GradeColor;
        [SerializeField]
        public Color[] TrashColor;
        [SerializeField]
        public Color[] BtnColor;
    }
}
